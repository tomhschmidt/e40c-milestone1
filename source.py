# audiocom library: Source and sink functions
import common_srcsink as common
import Image
from graphs import *
import binascii
import random
from Queue import PriorityQueue
import struct

MONOTONE = '00'
TEXT = '11'
IMAGE = '10'

MAX_PAYLOAD_LENGTH = 16
SYMBOL_LENGTH = 8
MAX_ENCODED_LENGTH = 5
MSG_LENGTH_NUM_BITS = 32

# MAX_TABLE_LENGTH = 16

class Tree(object):
    def __init__(self):
        self.left = None
        self.right = None
        self.symbol = None
        self.prob = None

# Format of Header:
# (32 bits for total size of message including the 32) (2 Bits for type) (16 bits for length) (1 bit for compress option) (x bits for table)
# Determine x: len(message) - 169 - payload_length
# Huffman - Symbol : Length of encoded symbol : encoded symbol
class Source:
    def __init__(self, monotone, filename=None, compress=False, encryptcompress=False):
        self.monotone = monotone
        self.fname = filename
        self.compress = compress
        self.encryptcompress= encryptcompress
        print 'Source: '

    
    def hex_str_to_binary_str(self, hex_str):
        hex_dict = {}
        bin_str = ''
        hex_dict['0'] = '0000'
        hex_dict['1'] = '0001'
        hex_dict['2'] = '0010'
        hex_dict['3'] = '0011'
        hex_dict['4'] = '0100'
        hex_dict['5'] = '0101'
        hex_dict['6'] = '0110'
        hex_dict['7'] = '0111'
        hex_dict['8'] = '1000'
        hex_dict['9'] = '1001'
        hex_dict['a'] = '1010'
        hex_dict['b'] = '1011'
        hex_dict['c'] = '1100'
        hex_dict['d'] = '1101'
        hex_dict['e'] = '1110'
        hex_dict['f'] = '1111'
        for char in hex_str:
            bin_str += hex_dict[char]
        return bin_str

    # Citing ActiveState from Paul Chakravarti:
    # Used the code as a starting point but also paired
    # with Wikipedia articles to better understand tea encryption
    def tea_encrypt(self, data):
        key = '0123456789012345'
        iv = 'ABCDEFGH'
        num_rounds = 32
        encryption_str = ''

        xor = [ chr(x^y) for (x,y) in zip(map(ord,data),self.keygen(key,iv,num_rounds)) ]
        return self.hex_str_to_binary_str("".join(xor).encode('hex'))

    def xtea_encrypt(self, key, block, n=32, endian="!"):
        v0,v1 = struct.unpack(endian+"2L",block)
        k = struct.unpack(endian+"4L",key)
        sum,delta,mask = 0L,0x9e3779b9L,0xffffffffL
        for round in range(n):
            v0 = (v0 + (((v1<<4 ^ v1>>5) + v1) ^ (sum + k[sum & 3]))) & mask
            sum = (sum + delta) & mask
            v1 = (v1 + (((v0<<4 ^ v0>>5) + v0) ^ (sum + k[sum>>11 & 3]))) & mask
        return struct.pack(endian+"2L",v0,v1) 
               
    def keygen(self, key,iv,n):
        while True:
            iv = self.xtea_encrypt(key,iv,n)
            for k in iv:
                yield ord(k)

    def process(self):
        src_type = ''
        payload_bit_str = ''
        payload_bits = None
        # Form the databits, from the filename
        if self.fname is not None:
            if self.fname.endswith('.png') or self.fname.endswith('.PNG'):
                src_type = 'image'
                payload_bit_str = self.bits_from_image(self.fname)
                payload_bits = common.binary_array_from_str(payload_bit_str)
            else:
                src_type = 'text'
                payload_bit_str = self.text2bits(self.fname)
                payload_bits = common.binary_array_from_str(payload_bit_str)
        else:
            src_type = 'monotone'
            payload_bits = numpy.ones(self.monotone)
        payload_length = len(payload_bits)

        # Set source bits before we decide to compress or not
        src_bits = payload_bits

        # Create data_bits by concatenating header and payload
        header = None
        header_ext = None

        # Simply prints information
        orig_payload_bit_str = payload_bit_str
        if self.encryptcompress == True:
            print '\t---- Running Extra Credit ----'

            # Encrypt then Compress
            encrypted_payload_bit_str = self.tea_encrypt(orig_payload_bit_str)
            (compressed_payload_bits, dummy) = self.huffman_encode(encrypted_payload_bit_str)
            print '\tOriginal Length: ' + str(len(orig_payload_bit_str))
            print '\tEncrypt length: ' + str(len(encrypted_payload_bit_str))
            print '\tEncrypt then compress length: ' + str(len(compressed_payload_bits))
            print '\tCompression rate for encrypt then compress: ' + str(len(compressed_payload_bits) * 1.0 / len(orig_payload_bit_str)) # fill in here

            # Compress then encrypt
            (compressed_payload_bits, dummy) = self.huffman_encode(orig_payload_bit_str)
            compressed_payload_bit_str = ''
            for i in range(len(compressed_payload_bits)):
                if compressed_payload_bits[i] == 0:
                    compressed_payload_bit_str += '0'
                else:
                    compressed_payload_bit_str += '1'
            encrypted_payload_bit_str = self.tea_encrypt(compressed_payload_bit_str)
            print '\tOriginal Length: ' + str(len(orig_payload_bit_str))
            print '\Compressed length: ' + str(len(compressed_payload_bit_str))
            print '\tCompress then encrypt length: ' + str(len(encrypted_payload_bit_str))
            print '\tCompression rate for compress then encrypt: ' + str((1.0 * len(encrypted_payload_bit_str) / len(orig_payload_bit_str))) # fill in here
            print '\t---- Finished Extra Credit ----'
            exit(0)
        # Proceeds as normal
        if self.compress and self.fname is not None:
            # Just to ensure monotone doesn't compress
            # Perform Huffman coding if the compression option is on
            # compress will be to be False if you send monotone
            # (handled in sendrecv.py
            (payload_bits, huffman_table) = self.huffman_encode(payload_bit_str)
            payload_length = len(payload_bits)
            (header, header_ext) = self.get_header(payload_length, src_type, huffman_table)
            data_bits = numpy.concatenate([header_ext, payload_bits])
            print "\tHeader length: ", str(len(header_ext)) # fill in here
        else:
            (header, header_ext) = self.get_header(payload_length, src_type, None)
            data_bits = numpy.concatenate([header, payload_bits])
            print "\tHeader length: ", str(len(header)) # fill in here
        print '\tSource type: ' + src_type # fill in here
        print '\tPayload Length: ' + str(payload_length) # fill in here
        print '\tHeader: ' + str(header) # fill in here (exclude the extension)

        # srcbits is the bit sequence representing source data
        # payload is the data part that is sent over the channel
        # databits is the bit sequence that is sent over the channel (including header)
        return src_bits, payload_bits, data_bits

    # Referenced from ActiveState Code's Website
    # Convert a byte string to it's hex string representation e.g. for output.
    def ByteToHex(self, byteStr ):
        return ''.join( [ "%02X " % ord( x ) for x in byteStr ] ).strip()

    def binary_array_from_str(self, binary_str):
        binary_array = numpy.zeros(len(binary_str))
        for i in range(len(binary_str)):
            if (binary_str[i] == '1'):
                binary_array[i] = 1
        return binary_array

    def ascii_str_to_binary_str(self, file_str):
        binary_str = ''
        for i in range(len(file_str)):
            ascii_byte = self.pure_binary(bin(ord(file_str[i])))
            while (len(ascii_byte) != 8):
                ascii_byte = '0' + ascii_byte
            binary_str += ascii_byte
        return binary_str

    def text2bits(self, filename):
        # Given a text file, convert to bits
        f = open(filename, 'r')
        return self.ascii_str_to_binary_str(f.read())

    def bits_from_image(self, filename):
        im = Image.open(filename)
        im = im.convert('L')
        image_str = im.tostring()
        assert len(image_str) == 1024
        return self.ascii_str_to_binary_str(image_str)

    # Removes '0b' portion of the binary string
    def pure_binary(self, binary_string):
        return binary_string[2:]

    def get_header(self, payload_length, srctype, stat_binary_array=None):
        # Given the payload length and the type of source
        # (image, text, monotone), form the header
        # Add header-extension if needed
        header_bit_str = ''
        if srctype == 'monotone':
            header_bit_str = MONOTONE
        elif srctype == 'text':
            header_bit_str = TEXT
        elif srctype == 'image':
            header_bit_str = IMAGE
        else:
            print 'Not defined'
        # 16 bit length payload length
        payload_length_bit_str = self.pure_binary(bin(payload_length))
        while (len(payload_length_bit_str) != MAX_PAYLOAD_LENGTH):
            payload_length_bit_str = '0' + payload_length_bit_str
        # Compression bit
        if(self.compress):
            header_bit_str = header_bit_str + payload_length_bit_str + '1'
        else:
            header_bit_str = header_bit_str + payload_length_bit_str + '0'
        # Create numpy array to send back
        header = common.binary_array_from_str(header_bit_str)
        header_ext = None
        # Add table to end if compression is on
        if self.compress:
            header_ext = numpy.concatenate([header, stat_binary_array])

        header_length = 0
        if (self.compress):
            header_length = len(header_ext) + MSG_LENGTH_NUM_BITS
        else:
            header_length = len(header) + MSG_LENGTH_NUM_BITS
        data_length_bit_str = self.pure_binary(bin(header_length + payload_length))
        while (len(data_length_bit_str) != MSG_LENGTH_NUM_BITS):
            data_length_bit_str = '0' + data_length_bit_str

        header = numpy.concatenate([common.binary_array_from_str(data_length_bit_str), header])
        if (self.compress):
            header_ext = numpy.concatenate([common.binary_array_from_str(data_length_bit_str), header_ext])
        return (header, header_ext)

    def create_code_dict(self, code_dict, root, curr_bit_str):
        if root.left is None and root.right is None:
            code_dict[root.symbol] = curr_bit_str
        else:
            self.create_code_dict(code_dict, root.left, curr_bit_str + '0')
            self.create_code_dict(code_dict, root.right, curr_bit_str + '1')

    def huffman_encode(self, srcbits):
        # Given the source bits, get the statistics of the symbols
        # Compress using huffman coding
        # Return statistics and the source-coded bits
        num_symbols = len(srcbits) / SYMBOL_LENGTH
        assert num_symbols * SYMBOL_LENGTH == len(srcbits)

        stat_dict = {}
        total_count = 0
        for i in range(num_symbols):
            total_count += 1
            symbol = str(srcbits[i*SYMBOL_LENGTH:i*SYMBOL_LENGTH + SYMBOL_LENGTH])
            if symbol in stat_dict:
                stat_dict[symbol] += 1
            else:
                stat_dict[symbol] = 1

        # Initialize Priority Queue
        pqueue = PriorityQueue()
        for symbol,count in stat_dict.items():
            node = Tree()
            node.symbol = symbol
            node.prob = count/total_count
            pqueue.put(node, node.prob)
        # Go through pairs until we have a single tree
        while (pqueue.qsize() > 1):
            nodeL = pqueue.get()
            nodeR = pqueue.get()
            newNode = Tree()
            newNode.left = nodeL
            newNode.right = nodeR
            newNode.prob = nodeL.prob + nodeR.prob
            pqueue.put(newNode, newNode.prob)
        code_dict = {}
        root = pqueue.get()
        if root.left is None and root.right is None:
            code_dict[root.symbol] = '0'
        else:
            self.create_code_dict(code_dict, root, '')

        encoded_bit_str = ''
        for i in range(num_symbols):
            symbol = str(srcbits[i*SYMBOL_LENGTH:i*SYMBOL_LENGTH + SYMBOL_LENGTH])
            encoded_bit_str += code_dict[symbol]

        # Create header stat information
        stat_bit_str = ''
        for symbol,code in code_dict.items():
            stat_bit_str += symbol
            code_length_bit_str = self.pure_binary(bin(len(code)))
            assert len(code_length_bit_str) <= MAX_ENCODED_LENGTH
            while (len(code_length_bit_str) < MAX_ENCODED_LENGTH):
                code_length_bit_str = '0' + code_length_bit_str
            stat_bit_str += code_length_bit_str
            stat_bit_str += code

        print "\tSource bit length: " + str(len(srcbits)) + "\n", # fill in here
        print "\tSource-coded bit length: " + str(len(encoded_bit_str)) + "\n", # fill in here
        print "\tCompression rate: ", str((1.0 * len(encoded_bit_str))/len(srcbits)) # fill in here
        return common.binary_array_from_str(encoded_bit_str), common.binary_array_from_str(stat_bit_str)