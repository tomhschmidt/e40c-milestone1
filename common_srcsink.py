import numpy
import math
import operator

def binary_array_from_str(binary_str):
    binary_array = numpy.zeros(len(binary_str))
    for i in range(len(binary_str)):
        if (binary_str[i] == '1'):
            binary_array[i] = 1
    return binary_array

# Methods common to both the transmitter and receiver.
def hamming(s1,s2):
    # Given two binary vectors s1 and s2 (possibly of different 
    # lengths), first truncate the longer vector (to equalize 
    # the vector lengths) and then find the hamming distance
    # between the two. Also compute the bit error rate  .
    # BER = (# bits in error)/(# total bits )
    if type(s1) == str:
      s1 = binary_array_from_str(s1)
    if type(s2) == str:
      s2 = binary_array_from_str(s2)

    min_len = len(s1) if len(s1) < len(s2) else len(s2)
    hamming_d = 0
    temp_array = numpy.zeros(min_len)
    if len(s1) < len(s2):
      for i in range(min_len):
        temp_array[i] = s2[i]
      s2 = temp_array
    if len(s2) < len(s1):
      for i in range(min_len):
        temp_array[i] = s1[i]
      s1 = temp_array

    for i in range(min_len):
      if (s1[i] != s2[i]):
        hamming_d += 1
    ber = (hamming_d * 1.0) / min_len
    return hamming_d, ber
