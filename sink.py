# audiocom library: Source and sink functions
import common_srcsink
import Image
from graphs import *
import binascii
import random
import numpy

SYMBOL_SIZE = 8
CODE_SIZE_LENGTH = 5

MONOTONE = 0
TEXT = 3
IMAGE = 2

HEADER_SIZE = 51 # Used to be 19

class Sink:
    def __init__(self, compression):
        self.compression = compression
        print 'Sink:'

    def stringtobits(self, s):
        result = []
        for c in s:
            bits = bin(ord(c))[2:]
            bits = '00000000'[len(bits):] + bits
            result.extend([int(b) for b in bits])
        return result

    def bitstostring(self, bits):
        #bits = bits.tolist()
        #print bits 
        chars = []
        for b in range(len(bits) / 8):
            byte = bits[b*8:(b+1)*8]
            char = int(''.join([str(int(bit)) for bit in byte]), 2)
            chars.append(chr(char))
        return ''.join(chars)

    def bitstobitstring(self, bits):
        bitstring = ''.join([str(int(bit)) for bit in bits])
        return bitstring

    def bitstringtobits(self, bitstring):
        bits = []
        for bit in bitstring:
            bits.append(int(bit))
        return bits

    def bitstringtoasciistring(self, bitstring):
        chars = []
        for b in range(len(bitstring) / 8):
            byte = bitstring[b*8:(b+1)*8]
            chars.append(chr(int(''.join([str(int(bit)) for bit in byte]), 2)))
        return ''.join(chars)

    def bitstoint(self, bits):
        bitstring = ""
        for bit in bits:
            if bit == 0:
                bitstring += "0"
            else:
                bitstring += "1"

        return int(bitstring, 2)

    def process(self, recd_bits):
        # Process the recd_bits to form the original transmitted
        # file. 
        # Here recd_bits is the array of bits that was 
        # passed on from the receiver. You can assume, that this 
        # array starts with the header bits (the preamble has 
        # been detected and removed). However, the length of 
        # this array could be arbitrary. Make sure you truncate 
        # it (based on the payload length as mentioned in 
        # header) before converting into a file.
        srctype, payload_length, stat, data_size = self.read_header(recd_bits)
        if (data_size != len(recd_bits)):
            print "Corruption in data, number of bits doesn't match up to header metadata"
            return ""
        sourceEndIndex = len(recd_bits) - payload_length
        srcbits = recd_bits[sourceEndIndex:]

        compressed = recd_bits[HEADER_SIZE - 1]
        if compressed:
            srcbits = self.huffman_decode(srcbits, stat)

        # If its an image, save it as "rcd-image.png"
        # If its a text, just print out the text
        
        if srcbits != None:
            print '\tRecd', len(srcbits) , 'source bits'
            if srctype == TEXT:
                textToPrint = ""
                if compressed:
                    textToPrint = self.bitstringtoasciistring(srcbits)
                else:
                    textToPrint = self.bits2text(srcbits)
                if textToPrint:
                    print "\tText recd:",textToPrint
            elif srctype == IMAGE:
                if compressed:
                    srcbits = self.bitstringtobits(srcbits)
                self.image_from_bits(srcbits, "rcd-image.png")

            return srcbits
        else:
            return ""


    def bits2text(self, bits):
        # Convert the received payload to text (string)
        text = self.bitstostring(bits)
        return  text

    def image_from_bits(self, bits,filename):
        # Convert the received payload to an image and save it
        # No return value required .
        chars = self.bitstostring(bits)
        image = Image.fromstring("L", (32,32), chars)
        image.save(filename)
        pass

    def read_header(self, header_bits): 
        # Given the header bits, compute the payload length
        # and source type (compatible with get_header on source)
        # Get information for decompression if needed
        data_size = header_bits[:32]
        data_size = self.bitstoint(data_size)
        header_bits = header_bits[32:]
        srctype = header_bits[0:2:1]
        srctype = self.bitstoint(srctype)
        payload_length = header_bits[2:18:1]
        payload_length = self.bitstoint(payload_length)
        compressed = header_bits[18]
        sourceEndIndex = len(header_bits) - payload_length

        stat = {}
        if compressed:
            curIndex = 19
            while curIndex < sourceEndIndex:
                curVal = self.bitstobitstring(header_bits[curIndex:curIndex+SYMBOL_SIZE:1])
                curCodeLength = self.bitstoint(header_bits[curIndex+SYMBOL_SIZE:curIndex+SYMBOL_SIZE+CODE_SIZE_LENGTH:1])
                curCode = self.bitstobitstring(header_bits[curIndex+SYMBOL_SIZE+CODE_SIZE_LENGTH:curIndex+SYMBOL_SIZE+CODE_SIZE_LENGTH+curCodeLength:1])
                
                stat[curCode] = curVal
                curIndex = curIndex + SYMBOL_SIZE +CODE_SIZE_LENGTH + curCodeLength

        srcTypeString = ""
        if srctype == MONOTONE:
            srcTypeString = "monotone"
        elif srctype == TEXT:
            srcTypeString = "text"
        else:
            srcTypeString = "image"
 
        print '\tRecd header: ', header_bits[0:18:1]
        print '\tSource type: ', srcTypeString
        print '\tLength from header: ', payload_length
        return srctype, payload_length, stat, data_size

    def huffman_decode(self, codedbits, stat):
        curString = ""
        srcbits = ""
        bitCounter = 0
        for bit in codedbits:
            bitCounter += 1
            curString += str(int(bit))
            if curString in stat:
                srcbits += stat[curString]
                curString = ""
            elif bitCounter == len(codedbits):
                print "\tError: detected compression corruption"
                print "\t(Bit sequence cannot be found in codebook)"
                return None
        return srcbits
